**my-workstation** builds my setup using ansible.

Supports:

-   Alpine Linux
-   Arch Linux
-   Debian stable
-   PureOS
-   Ubuntu LTS

Usage

```
ansible-playbook -i hosts pq.yml --ask-vault-pass
```

## Use cases

When I get my new laptop, I may use this to bootstrap my work environment.
