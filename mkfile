ANSIBLE_VAULT_PASSWORD_FILE=~/xihh/cmd/vault-password-file
ANSIBLE_FORCE_COLOR=true

help:QV:	## show this message
	awk 'BEGIN {FS = "\t|:.*?## "};
	/[ \t]##[ \t]/ {printf "\033[36m%-20s\033[0m %s\n", $1, $NF}' \
		mkfile \
	| sort

start:QV:	## setup development environment
	virtualenv .venv
	. .venv/bin/activate
	python -m pip install --upgrade pip
	python -m pip install -r requirements.txt
	molecule create

test:QV:	## test roles using molecule
	. .venv/bin/activate
	molecule -v --debug test --destroy=never \
	| tee /dev/fd/2 \
	| s6-log -b t s1000000 n20 ./logs
	molecule -v --debug converge --destroy=never \
	| tee /dev/fd/2 \
	| s6-log -b t s1000000 n20 ./logs

container:V:	## build container images
	buildah build -t "registry.gitlab.com/xihh87/my-workstation/arch-s6-ansible:master" "docker/arch/s6"
	buildah build -t "registry.gitlab.com/xihh87/my-workstation/arch-systemd-ansible:master" "docker/arch/systemd"
	buildah build -t "registry.gitlab.com/xihh87/my-workstation/alpine-s6-ansible:master" "docker/alpine"
	buildah build -t "registry.gitlab.com/xihh87/my-workstation/pureos-ansible:master" "docker/pureos"

push:V:	docker	## build docker images
	podman push "registry.gitlab.com/xihh87/my-workstation/arch-s6-ansible:master"
	podman push "registry.gitlab.com/xihh87/my-workstation/alpine-s6-ansible:master"
	podman push "registry.gitlab.com/xihh87/my-workstation/pureos-ansible:master"
