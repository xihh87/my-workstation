Role Name
=========

Restrict SSH capabilities to make more difficult getting access to your server.

Requirements
------------

You should have ssh keys configured to access your users. Otherwise you'll be locked out.

Role Variables
--------------

The variable of this role are namespaced so they do not colide with other roles.

-   `ports` are ports allowed for your server.

    it is recommended that you enable a port different than 22,
    but on first pass maybe you want to leave it enabled
    to make sure you don't get locked out of your server.

-   `only_stfp`: can configure users or groups to only allow file exchange
    they are chrooted to their home by default.

    You may need to remove write permissions for parent directories
    of the user's home.

-   `allow_forwarding`: forwarding is disabled by default,
    but you can configure users or groups to allow forwarding of

    -   `tcp`, `x11`, `agent`, `stream`
        See `man 5 sshd_config` for details.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
---
- hosts: servers
  vars:
    - hardening_ssh:
        ports:
          - 22
          - 1281
        only_sftp:
          groups:
            - uucp
          users:
            - ftp
        allow_forwarding:
          tcp:
            users:
              - user
          x11:
            group:
              - groups
          agent:
            users:
              - user
  roles:
    - role: hardening_ssh
```

License
-------

BSD

Author Information
------------------

<joshua.haase.mx>
