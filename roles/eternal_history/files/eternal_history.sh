### https://unix.stackexchange.com/a/359217/238042 ###
# Consistent and forever bash history
HISTSIZE=100000
HISTFILESIZE=$HISTSIZE
HISTCONTROL=ignorespace:ignoredups

_bash_history_sync() {
  builtin history -a         #1
}

_bash_history_sync_and_reload() {
  _bash_history_sync
  builtin history -c         #3
  builtin history -r         #4
}

history() {                  #5
  _bash_history_sync_and_reload
  builtin history "$@"
}

export HISTTIMEFORMAT="%Y-%m-%dT%H:%M:%S        "
PROMPT_COMMAND="${PROMPT_COMMAND:-true};_bash_history_sync;echo \$\$	\${USER}	\${PWD}:	\$(history 1)>>${HOME}/.bash_eternal_history"
