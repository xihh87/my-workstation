// Debian preferences
pref("extensions.update.enabled", true);
// Use LANG environment variable to choose locale
pref("intl.locale.requested", "");
// Disable default browser checking.
pref("browser.shell.checkDefaultBrowser", false);
// Disable openh264.
pref("media.gmp-gmpopenh264.enabled", false);
// Default to classic view for about:newtab
pref("browser.newtabpage.enhanced", false, sticky);
// Disable health report upload
pref("datareporting.healthreport.uploadEnabled", false);
// Default to no suggestions in the urlbar. This still brings a panel asking
// the user whether they want to opt-in on first use.
pref("browser.urlbar.suggest.searches", false);
pref("services.sync.prefs.sync.browser.urlbar.suggest.searches", false);
pref("browser.search.selectedEngine", "DuckDuckGo");
pref("browser.search.order.1", "DuckDuckGo");
pref("browser.search.defaultenginename", "DuckDuckGo");
pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.searchEngines", "DuckDuckGo");
pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned", "DuckDuckGo");
// Restore session on exit.
//
// http://kb.mozillazine.org/Browser.startup.page
// http://kb.mozillazine.org/Session_Restore
pref("browser.startup.page", 3);
// Do not load startup homepage (firefox-perfil.md)
pref("browser.startup.homepage", "about:blank");
pref("browser.tabs.loadInBackground", false);
// Circle tabs on displayed order
pref("browser.ctrlTab.recentlyUsedOrder", false);
// Do not use downloads directory
pref("browser.download.useDownloadDir", false);
// Privacy preferences created with https://ffprofile.com/
pref("app.normandy.api_url", "");
pref("app.normandy.enabled", false);
pref("app.shield.optoutstudies.enabled", false);
pref("app.update.auto", false);
pref("beacon.enabled", false);
pref("breakpad.reportURL", "");
pref("browser.cache.offline.enable", false);
pref("browser.crashReports.unsubmittedCheck.autoSubmit", false);
pref("browser.crashReports.unsubmittedCheck.autoSubmit2", false);
pref("browser.crashReports.unsubmittedCheck.enabled", false);
pref("browser.disableResetPrompt", true);
pref("browser.fixup.alternate.enabled", false);
pref("browser.newtab.preload", false);
pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
pref("browser.newtabpage.enabled", false);
pref("browser.newtabpage.enhanced", false);
pref("browser.newtabpage.introShown", true);
pref("browser.safebrowsing.appRepURL", "");
pref("browser.safebrowsing.blockedURIs.enabled", false);
pref("browser.safebrowsing.downloads.enabled", false);
pref("browser.safebrowsing.downloads.remote.enabled", false);
pref("browser.safebrowsing.downloads.remote.url", "");
pref("browser.safebrowsing.enabled", false);
pref("browser.safebrowsing.malware.enabled", false);
pref("browser.safebrowsing.phishing.enabled", false);
pref("browser.selfsupport.url", "");
pref("browser.send_pings", false);
pref("browser.shell.checkDefaultBrowser", false);
pref("browser.startup.homepage_override.mstone", "ignore");
pref("browser.tabs.crashReporting.sendReport", false);
pref("browser.urlbar.speculativeConnect.enabled", false);
pref("browser.urlbar.trimURLs", false);
pref("datareporting.healthreport.service.enabled", false);
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.policy.dataSubmissionEnabled", false);
pref("dom.battery.enabled", false);
pref("dom.event.clipboardevents.enabled", false);
pref("dom.webaudio.enabled", false);
pref("experiments.activeExperiment", false);
pref("experiments.enabled", false);
pref("experiments.manifest.uri", "");
pref("experiments.supported", false);
// Disable extensions after silent updates
// the most secure position may be:
//   pref("extensions.autoDisableScopes", 14);
// however, I install extensions silently on my config
pref("extensions.autoDisableScopes", 0);
pref("extensions.getAddons.cache.enabled", false);
pref("extensions.getAddons.showPane", false);
pref("extensions.greasemonkey.stats.optedin", false);
pref("extensions.greasemonkey.stats.url", "");
pref("extensions.pocket.enabled", false);
pref("extensions.shield-recipe-client.api_url", "");
pref("extensions.shield-recipe-client.enabled", false);
pref("extensions.webservice.discoverURL", "");
pref("general.warnOnAboutConfig", false);
pref("media.autoplay.default", 1);
pref("media.autoplay.enabled", false);
pref("media.navigator.enabled", false);
pref("media.peerconnection.enabled", false);
pref("media.video_stats.enabled", false);
pref("network.allow-experiments", false);
pref("network.captive-portal-service.enabled", false);
pref("network.cookie.cookieBehavior", 1);
pref("network.dns.disablePrefetch", true);
pref("network.http.referer.spoofSource", true);
pref("network.http.speculative-parallel-limit", "0");
pref("network.prefetch-next", false);
pref("network.trr.mode", 5);
pref("privacy.donottrackheader.enabled", true);
pref("privacy.donottrackheader.value", 1);
pref("privacy.trackingprotection.enabled", true);
pref("privacy.trackingprotection.pbmode.enabled", true);
pref("privacy.usercontext.about_newtab_segregation.enabled", true);
pref("security.ssl.disable_session_identifiers", true);
pref("signon.autofillForms", false);
// Do not remember firefox password as I use pass
// https://serverfault.com/questions/336608/disable-save-password-in-firefox-by-group-policy
pref("signon.rememberSignons", false);
pref("webgl.disabled", true);
pref("webgl.renderer-string-override", " ");
pref("webgl.vendor-string-override", " ");
